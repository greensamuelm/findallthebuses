$(function () {
//These are the routes near the office
//19, 12, 27, 47

callForBuses();

function callForBuses() {
	getPredictions('19', '19_OB1', '3206');
	getPredictions('19', '19_IB1', '3193');

	getPredictions('12', '12_OB1', '4971');
	getPredictions('12', '12_IB1', '4661');

	getPredictions('27', '27_OB2', '4971');
	getPredictions('27', '27_IB1', '3726');

	getPredictions('47', '47_OB', '3726');
	getPredictions('47', '47_IB', '4971');
	
	//Will show error condition if uncommented
	//getPredictions('XX', '08X__IB', '3725');
	
	var timeout = setTimeout(callForBuses, 3000);
}

function getPredictions(routetag, directiontag, stoptag) {
	
	var predictionUrl = 'http://webservices.nextbus.com/service/publicXMLFeed?command=predictions&a=sf-muni&r=<route tag>&d=<direction tag>&s=<stop tag>';
	
	predictionUrl = predictionUrl.replace('<route tag>', routetag);
	predictionUrl = predictionUrl.replace('<direction tag>', directiontag);
	predictionUrl = predictionUrl.replace('<stop tag>', stoptag);
	
	$.ajax({
		url: predictionUrl,
		type: "GET",
		dataType: "xml",
		success: parseXmlPredictions,
		failure: function() {
			var routeId = routeTag + stopTag;
			errors(routeId);
		},
		error: function() {
			var routeId = routeTag + stopTag;
			errors(routeId);
		}
	});
}

function errors(routeId) {

	var container = $('#main').find('#container' + routeId + " .chart");
	
	if(container.length) {
		container.empty()
		container.append('<img src="allthebuses.png" style="width: 400px; height = 386px;"/>');
		container.css('height', '386px');
	} else {
		$('#main').append('<div class="container" id=container' + routeId + '>');
	}
}

function parseXmlPredictions(xml) {

	//Route by itself is the same in both directions, need a combo to make an Id
	var routeId = $(xml).find('predictions').attr('routeTag') + $(xml).find('predictions').attr('stopTag');

	var container = $('#main').find('#container' + routeId);
	
	if(!container.length) {
		container = $('#main').append('<div class="container" id=container' + routeId + '>');
	}
	else {
		container.empty();
	}
	
	$('#container' + routeId).append('<div class="routeTitle" id=routeTitle' + routeId + '/>');
	$('#container' + routeId).append('<div class="directionTitle" id=directionTitle' + routeId + '/>');
	$('#container' + routeId).append('<div class="stopTitle" id=stopTitle' + routeId + '/>');
	$('#container' + routeId).append('<div class="chart" id=timeChart' + routeId + '/>');
	
	var routeTitle = $(xml).find('predictions').attr('routeTitle');
	$('#routeTitle' + routeId).text(routeTitle);
	var stopTitle = $(xml).find('predictions').attr('stopTitle');
	$('#stopTitle' + routeId).text(stopTitle);
	var directionTitle = $(xml).find('direction').attr('title');
	$('#directionTitle' + routeId).text(directionTitle);
	
	
	var predictions = $(xml).find('prediction');
	
	if(!predictions.length){
		errors(routeId);
	} else {
		createChart(predictions, routeId);
	}
	
	
	
	//close the container
	$('#main').append('</div>');

}

function createChart(predictions, routeId) {
	
	var data = [];
	//Used for calculating the pie chart
	var lastMinutes = 0;
	$(predictions).each( function(index) {
		var stringMinutes = $(this).attr('minutes');
		
		//I only want the buses in the next hour
		var intMinutes = parseInt(stringMinutes);
	
		if( intMinutes < 60 ) {
			var color = '#D1F4FF';
			
			if(intMinutes < 6) {
				color = '#FF0000';
			} else if(intMinutes < 11) {
				color = '#FFFF21';
			} else if(intMinutes < 16) {
				color = '#1BE032';
			}
			
			
			data.push({label: stringMinutes + " min", data: (intMinutes - lastMinutes), color: color});
			lastMinutes = intMinutes;
		}
	});
	
	if(data.length > 0) {
		if(lastMinutes < 60) {
			data.push({label: "", data: (60 - lastMinutes), color: '#D1F4FF'});
		}		
		$.plot($('#timeChart' + routeId), data, {
			series: {
				pie: { 
					show: true
				},
				
				}
			});
		} else {
			errors(routeId);
		}	
}
//API Key RLHD-TYE6-TQN6-U8TK for sshkuratoff@atlassian.com
function getBARTPrediction() {
	
	var predictionUrl = 'http://api.bart.gov/api/etd.aspx?cmd=etd&orig=CIVC&key=RLHD-TYE6-TQN6-U8TK';
	
	$.ajax({
		url: predictionUrl,
		type: "GET",
		dataType: "xml",
		success: parseXmlPredictions,
		failure: function() {
		},
		error: function() {
		}
	});
}

function parseBARTPrediction(xml) {
	
	//List of departures
	var departures = $(xml).find('etd');
	
}

 
});
